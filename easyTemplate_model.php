<?php 

	// **
	// * 
	// **
	function print_dump($var){
		echo '<pre>'.print_r($var,true).'</pre>';
	}
	
	// **
	// *  Clase para almacenar 
	// **
	class Block
	{
		public $row;
		public $container;

		function __construct($row, $container)
		{
			$this->row = $row;
			$this->container = $container;
		}
	}

	// **
	// * Clase para el analisis de una tpl
	// **
	class EasyTemplate
	{

		private $template; // almacena el contenido de la tpl
		private $template_final; // contiene la tpl alterada
		private $block = array(); // almacena los bloques con su contenido
		
		// **
		// * Constructor, pide la url de la tpl y pasa el contenido del archivo a 2 variables.
		// **
		function __construct($tpl_url)
		{
			$this->template = file_get_contents($tpl_url);
			$this->template_final = $this->template;
		}

		// **
		// * Analiza la tpl que fue cargada en memoria para buscar los bloques.
		// **
		public function analize(){

			// busco los start de los block
			preg_match_all('/<!-- START-BLOCK : (.*) -->/', $this->template, $start_block);

			//print_dump($start_block);

			for ($i=0; $i < count($start_block[0]); $i++) {

				$pos_start_block = strpos($this->template, $start_block[0][$i])+strlen('<!-- START-BLOCK : '.$start_block[1][$i].' -->');

				$pos_end_block = strpos($this->template, '<!-- END-BLOCK : '.$start_block[1][$i].' -->');

				// var_dump($pos_end_block);
				$name = $start_block[1][$i];
				
				// posicion del caracter donde inicia el bloque
				$row = $pos_start_block;

				// extrae el texto contenido entre el START y el END block
				$container = substr($this->template, $pos_start_block, $pos_end_block-$pos_start_block);

				// almacena los datos del bloque en un arreglo
				$this->block[$name] = new Block($row, $container);


			}

			//print_dump($this->block);

			// Ordena 
			$start_block[0] = array_reverse($start_block[0]);
			$start_block[1] = array_reverse($start_block[1]);

			for ($i=0; $i < count($start_block[0]); $i++) { 
						
				$pos_start_block = strpos($this->template_final, $start_block[0][$i])+strlen('<!-- START-BLOCK : '.$start_block[1][$i].' -->');
				$pos_end_block = strpos($this->template_final, '<!-- END-BLOCK : '.$start_block[1][$i].' -->');

				$this->template_final = substr_replace($this->template_final, "",$pos_start_block, $pos_end_block-$pos_start_block);
			}

			//print_dump($this->block);

			// analiza un bloque, si contien otro bloque en su interior lo limpia
			foreach ($start_block[1] as $key => $value) {
				$this->cleanTreeBlock($value);
				
			}

			//print_dump($this->block);
		}

		// **
		// * 
		// **
		private function cleanTreeBlock($block_name){
			
			
			preg_match_all('/<!-- START-BLOCK : (.*) -->/', $this->block[$block_name]->container, $clean_block);

			if(count($clean_block[0])>0){

				// echo $block_name;
				// print_dump($clean_block);
			
				$i = 0;

				// for ($i=0; $i < count($clean_block[0]); $i++) { 
							
					$pos_start_block = strpos($this->block[$block_name]->container, $clean_block[0][$i])+strlen('<!-- START-BLOCK : '.$clean_block[1][$i].' -->');
					$pos_end_block = strpos($this->block[$block_name]->container, '<!-- END-BLOCK : '.$clean_block[1][$i].' -->');

					$this->block[$block_name]->container = substr_replace($this->block[$block_name]->container, "",$pos_start_block, $pos_end_block-$pos_start_block);

					// print_dump($this->block[$block_name]);
				// }
			}
			
		}

		// **
		// * Genera el bloque dentro de la tpl virtual, esta es la que luego se va imprimir.
		// **
		public function newBlock(string $block_name, bool $final=false){

			$find_block = '<!-- START-BLOCK : '.$block_name.' --><!-- END-BLOCK : '.$block_name.' -->';

			if($final){
				$continue_block = "";
			}else{
				$continue_block = PHP_EOL . $find_block;
			}

			// deja la marca del bloque arriba de lo insertado, de esta forma se puede crear varias veces el bloque
			$this->template_final = str_replace($find_block, $this->block[$block_name]->container . $continue_block, $this->template_final);
		}

		// **
		// * Retorna un arreglo con los bloques disponibles
		// **
		public function getAllBlocks(){
			foreach ($this->block as $key => $value) {
				$buff[] = $key; 
			}

			return $buff;
		}

		// **
		// * Reemplaza la variable de la tpl ("VARIABLE" => {VARIABLE}) con el valor de la variable $var_replace
		// **
		public function assign(string $var_tpl, string $var_replace){

			$this->template_final = str_replace("{".$var_tpl."}", $var_replace, $this->template_final);
		}

		// **
		// * Imprime la tpl en el navegador, si se activa el modo debug 
		// **
		public function print(string $debug = 'NONE'){

			if($debug == 'DEBUG'){

				// Genera un archivo de texto con el contenido de la plantilla
				$f = fopen("result.txt","w");
				fwrite($f, $this->template_final);
				fclose($f);
	
			}


			if($debug == 'NONE'){

				// Elimina las marcas de los bloques
				$all_blocks = $this->getAllBlocks();

				for ($i=0; $i < count($all_blocks); $i++) { 
					
					$start_block = '<!-- START-BLOCK : '.$all_blocks[$i].' -->';

					$end_block = '<!-- END-BLOCK : '.$all_blocks[$i].' -->';

					$this->template_final = str_replace($start_block, "", $this->template_final);
					$this->template_final = str_replace($end_block, "", $this->template_final);
				}
			}
			
			echo $this->template_final;
		}

	}

 ?>